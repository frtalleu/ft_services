#!/bin/bash


#clean env
docker system prune -f
minikube delete --all
echo "user42" | sudo -S pkill nginx
sudo pkill mysqld

#give rights to docker
sudo chmod 666 /var/run/docker.sock

#start minikube
./srcs/minikube start --vm-driver=docker
eval $(./srcs/minikube -p minikube docker-env)
./srcs/minikube addons enable metallb
last=$(./srcs/minikube ip | sed "s!.*\.!!")
last=$((last + 1))
ip=$(./srcs/minikube ip | sed "s:[^.]*$:$last:")
sed  -i "12s/.*/      - $ip-$ip/" ./srcs/metallb/metallb.yaml
kubectl apply -f ./srcs/metallb/metallb.yaml

#deploy Nginx
docker build -t nginx ./srcs/nginx/
kubectl apply -f ./srcs/nginx/nginx.yaml

#deploy mysql
docker build -t mysql ./srcs/mysql/
kubectl apply -f ./srcs/mysql/mysql.yaml

#deploy phpmyadmin
docker build -t phpmyadmin ./srcs/phpmyadmin/
kubectl apply -f ./srcs/phpmyadmin/phpmyadmin.yaml

#deploy wordpress
docker build -t wordpress ./srcs/wordpress/
kubectl apply -f ./srcs/wordpress/wordpress.yaml

#deploy ftps
docker build -t ftps ./srcs/ftps/
kubectl apply -f ./srcs/ftps/ftps.yaml

#deploy influxdb
docker build -t influxdb ./srcs/influxdb/
kubectl apply -f ./srcs/influxdb/influxdb.yaml

#deploy telegraf
docker build -t telegraf ./srcs/telegraf/
kubectl apply -f ./srcs/telegraf/telegraf.yaml

#deploy grafana
docker build -t grafana ./srcs/grafana/
kubectl apply -f ./srcs/grafana/grafana.yaml

#get dashboard
./srcs/minikube dashboard

