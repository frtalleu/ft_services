service influxdb start

sleep 10

influx -execute "CREATE DATABASE influxdb" && \
influx -execute "CREATE USER frtalleu WITH PASSWORD 'frtalleu'" && \
influx -execute "GRANT ALL ON influxdb TO frtalleu"

tail -f /dev/null
