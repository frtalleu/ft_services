openssl req -x509 -nodes -subj '/CN=localhost' -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/server.key -out /etc/ssl/certs/server.crt
adduser -D -g 'www' www
chown -R www:www /var/lib/nginx
chown -R www:www /www

openrc
touch /run/openrc/softlevel
service nginx start
tail -f /dev/null
